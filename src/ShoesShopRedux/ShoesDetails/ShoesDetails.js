import React, { Component } from "react";
import { connect } from "react-redux";

class ShoesDetails extends Component {
  render() {
    return (
      <div className='row'>
        <img className='col-4' src={this.props.data.image} alt='true' />
        <div className='col-8'>
          <p>{this.props.data.name}</p>
          <p>{this.props.data.price}</p>
          <p>{this.props.data.shortDescription}</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.shoesReducer.details };
};

export default connect(mapStateToProps)(ShoesDetails);
