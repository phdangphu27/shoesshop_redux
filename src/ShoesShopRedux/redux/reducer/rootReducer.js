import { combineReducers } from "redux";
import { shoesReducer } from "./shoesReducer";

export const rootReducer_ShoesShop = combineReducers({ shoesReducer });
