import { dataShoes } from "../../dataShoes.js";
import {
  ADD_TO_CART,
  DETAILS,
  INCREMENT,
  DECREMENT,
} from "../constant/constant.js";

const initialState = {
  cart: [],
  shoesArr: dataShoes,
  details: dataShoes[0],
};

export const shoesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      const cloneCart = [...state.cart];

      const index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      if (index === -1) {
        cloneCart.push({ ...action.payload, quantity: 1 });
      } else {
        cloneCart[index].quantity++;
      }

      return { ...state, cart: cloneCart };
    }

    case DETAILS: {
      return { ...state, details: action.payload };
    }

    case INCREMENT: {
      const cloneCart = [...state.cart];

      const index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      cloneCart[index].quantity++;

      return { ...state, cart: cloneCart };
    }

    case DECREMENT: {
      const cloneCart = [...state.cart];

      const index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      cloneCart[index].quantity--;

      if (cloneCart[index].quantity === 0) {
        const newArr = cloneCart.filter((item) => {
          return item.id !== action.payload.id;
        });

        return { ...state, cart: newArr };
      } else {
        return {
          ...state,
          cart: cloneCart,
        };
      }
    }

    default:
      return state;
  }
};
