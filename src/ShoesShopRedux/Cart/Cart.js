import React, { Component } from "react";
import { connect } from "react-redux";
import { DECREMENT, INCREMENT } from "../redux/constant/constant";

class Cart extends Component {
  renderShoesCart = () => {
    console.log(this.props.data);
    return this.props.data.map((shoes) => {
      return (
        <tr>
          <td>{shoes.id}</td>
          <td>{shoes.name}</td>
          <td>{shoes.price * shoes.quantity}</td>
          <td>
            <button
              onClick={() => {
                this.props.btnReduceQuantity(shoes);
              }}
              className='btn btn-danger'
            >
              -
            </button>
            {shoes.quantity}
            <button
              onClick={() => {
                this.props.btnAddQuantity(shoes);
              }}
              className='btn btn-success'
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={shoes.image} alt='true' />
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className='table container'>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderShoesCart()}</tbody>
      </table>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.shoesReducer.cart };
};

const mapDispatchToProps = (dispatch) => {
  return {
    btnAddQuantity: (value) => {
      dispatch({
        type: INCREMENT,
        payload: value,
      });
    },
    btnReduceQuantity: (value) => {
      dispatch({
        type: DECREMENT,
        payload: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
