import React, { Component } from "react";
import { connect } from "react-redux";
import { shoesReducer } from "../redux/reducer/shoesReducer";
import ShoesItem from "./ShoesItem/ShoesItem";

class ShoesList extends Component {
  renderShoesList = () => {
    return this.props.data.map((shoes) => {
      return <ShoesItem data={shoes} />;
    });
  };

  render() {
    return (
      <div className='container'>
        <div className='row'>{this.renderShoesList()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.shoesReducer.shoesArr };
};

export default connect(mapStateToProps)(ShoesList);
