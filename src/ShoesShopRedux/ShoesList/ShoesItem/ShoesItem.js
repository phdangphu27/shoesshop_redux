import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, DETAILS } from "../../redux/constant/constant";

class ShoesItem extends Component {
  render() {
    return (
      <div className='col-lg-3 col-md-4 col-sm-6 p-2'>
        <div className='card h-100 text-left'>
          <img
            className='card-img-top'
            src={this.props.data.image}
            alt='Card cap'
          />
          <div className='card-body'>
            <h5 className='card-title'>{this.props.data.name}</h5>
            <p className='card-text'>{this.props.data.shortDescription}</p>
            <a
              href='##'
              onClick={() => {
                console.log(this.props.data);
                this.props.btnAddToCart(this.props.data);
              }}
              className='btn btn-primary'
            >
              Buy
            </a>
            <a
              href='##'
              onClick={() => {
                this.props.btnShowDetails(this.props.data);
              }}
              className='btn btn-primary'
            >
              Details
            </a>
          </div>
        </div>
      </div>
    );
  }
}

const mapDisPatchToProps = (dispatch) => {
  return {
    btnAddToCart: (shoesObj) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoesObj,
      });
    },
    btnShowDetails: (shoesObj) => {
      dispatch({
        type: DETAILS,
        payload: shoesObj,
      });
    },
  };
};

export default connect(null, mapDisPatchToProps)(ShoesItem);
