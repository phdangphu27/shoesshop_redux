import React, { Component } from "react";
import Cart from "./Cart/Cart";
import ShoesDetails from "./ShoesDetails/ShoesDetails";
import ShoesList from "./ShoesList/ShoesList";

export default class ShoesShopRedux extends Component {
  render() {
    return (
      <div>
        <Cart />
        <ShoesList />
        <ShoesDetails />
      </div>
    );
  }
}
