import "./App.css";
import ShoesShopRedux from "./ShoesShopRedux/ShoesShopRedux";

function App() {
  return (
    <div className='App'>
      <ShoesShopRedux />
    </div>
  );
}

export default App;
